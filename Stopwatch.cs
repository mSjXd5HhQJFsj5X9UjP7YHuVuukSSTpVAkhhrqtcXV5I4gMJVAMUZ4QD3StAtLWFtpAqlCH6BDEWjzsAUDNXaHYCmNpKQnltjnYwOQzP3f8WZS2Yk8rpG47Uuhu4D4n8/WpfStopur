﻿using System;

namespace WpfStopur {
	public class Stopwatch {
		private TimeSpan _duration;
		public TimeSpan Duration { 
			get => _duration;
			set => _duration = value;
		}
		
		private DateTime? _startTime;
		public DateTime? StartTime {
			get => _startTime;
			set => _startTime = value;
		}
		
		private TimeSpan _remainingTime;
		public TimeSpan RemainingTime {
			get => _remainingTime;
			set => _remainingTime = value;
		}
		
		private bool _isRunning;

		public bool IsRunning {
			get => _isRunning;
			set => _isRunning = value;
		}
	}
}